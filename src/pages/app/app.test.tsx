import { render } from '@testing-library/react';
import { App } from './app';

describe('App Container', () => {
    it('should have a header that says "React 101"', () => {
        const { getAllByRole } = render(<App />);
        const headings = getAllByRole('heading');
        expect(headings.length).toBe(1);
        expect(headings[0]).toHaveTextContent('React 101');
    });
});
