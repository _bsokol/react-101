import React, { FunctionComponent } from 'react';
import styles from './app.module.scss';

export const App: FunctionComponent = () => {
    return (
        <div className={styles['home']}>
            <h1>Welcome to React 101</h1>
            <p>
                We're going to use this as our starting point to learn React, Redux, and TypeScript
            </p>
        </div>
    );
};
